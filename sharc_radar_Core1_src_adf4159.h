/*
 * adf4159.h
 *
 *  Created on: 12.07.2017
 *      Author: roehr
 */

#ifndef ADF4159_H_
#define ADF4159_H_

#include <inttypes.h>
#include <adi_types.h>

// Analog Devices ADF4159 Registers
#define PLL_REG_R0_FRAC_INT		0x00    // Adress R0 , size 4 Bytes
#define PLL_REG_R1_LSB_FRAC		0x01 	// Adress R1 , size 4 Bytes
#define PLL_REG_R2_R_DIVIDER	0x02	// Adress R2 , size 4 Bytes
#define PLL_REG_R3_FUNCTION		0x03	// Adress R3 , size 4 Bytes
#define PLL_REG_R4_CLOCK		0x04	// Adress R4 , size 4 Bytes
#define PLL_REG_R5_DEVIATION	0x05	// Adress R5 , size 4 Bytes
#define PLL_REG_R6_STEP			0x06	// Adress R6 , size 4 Bytes
#define PLL_REG_R7_DELAY		0x07 	// Adress R7 , size 4 Bytes
// Sum of Registersize 32 Bytes

// todo: maybe define pins for CE/LE/trigger

// PLL_LE -> PC_12,MUX0 /SPI0_SEL3
// PLL_CE -> DAI1_PIN1
// PLL_TRG -> PB_10,MUX1 TM0_TMR2
// TR_EN -> DAI1_PIN2

#define PLL_FRAC_LSBS			13
#define PLL_FRAC_MSBS			12

struct adf4159State {
	uint32_t intValue;
	uint32_t fracMsb;
	uint32_t fracLsb;
	int32_t deviationWord;
	uint32_t stepWord;
	uint32_t clk1Div;
};

//PLL variable
struct ramp_config;
struct ramp;
enum sweep;

void adf4159Init(void);
void adf4159ConfigRamp(struct ramp_config *ramp_config, struct ramp *ramp);
void adf4159PrepareResponseRamp(struct ramp *ramp_in, struct ramp *ramp_out, float64_t offsets[2]);
void adf4159UpdateRampSettingsFast(const struct ramp *ramp, enum sweep sweep);

#endif /* ADF4159_H_ */
