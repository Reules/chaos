#include <stdio.h>

#define ELEMENTS(x)  (sizeof(x)/sizeof(*x))

typedef void (MenuFnct)( void );

struct MenuEntry
{
  char      Text[20];
  MenuFnct* Function;
};

void HandleMenu( const char* MenuTitle, struct MenuEntry* Menu,
unsigned char MenuSize )
{
  int i;
  int Auswahl;

  do {

    printf( "%s\n", MenuTitle );

    for( i = 0; i < MenuSize; ++i )
      printf( "%d: %s\n", i+1, Menu[i].Text );

    do {
      printf( "\nWahl: " );
      scanf( "%d", &Auswahl );
    } while( Auswahl < 1 || Auswahl > MenuSize );

    Auswahl--;

    if( Menu[Auswahl].Function ) {
      (*Menu[Auswahl].Function)();
      printf( "\n" );
    }

  } while( Menu[Auswahl].Function );
}

void HandleNew( void )
{
  printf( "Funktion 'Neu' ausgewählt\n" );
}

void HandleOpen( void )
{
  printf( "Funktion 'Öffnen' ausgewählt\n" );
}

void HandleSave( void )
{
  printf( "Funktion 'Speichern' ausgewählt\n" );
}

void HandleCut( void )
{
  printf( "Funktion 'Ausschneiden' ausgewählt\n" );
}

void HandleCopy( void )
{
  printf( "Funktion 'Kopieren' ausgewählt\n" );
}

void HandlePaste( void )
{
  printf( "Funktion 'Einfügen' ausgewählt\n" );
}

void HandleClose( void )
{
  printf( "Funktion 'Schjließen' ausgewählt\n" );
}

void SelectMenu( void )
{
  struct MenuEntry SelectMenu[] =
    { { "Zurück",    0 } };

  HandleMenu( "Auswählen", SelectMenu, ELEMENTS( SelectMenu ) );
}

void DateiMenu( void )
{
  struct MenuEntry FileMenu[] =
    { { "Neu",       HandleNew },
      { "Öffnen",    HandleOpen },
      { "Speichern", HandleSave },
      { "Zurück",    0 } };

  HandleMenu( "Datei", FileMenu, ELEMENTS( FileMenu ) );
}

void BearbeitenMenu( void )
{
  struct MenuEntry EditMenu[] =
    { { "Ausschneiden", HandleCut },
      { "Kopieren",     HandleCopy },
      { "Einsetzen",    HandlePaste },
      { "Zurück",    0 } };

  HandleMenu( "Bearbeiten", EditMenu, ELEMENTS( EditMenu ) );
}

void AnsichtMenu( void )
{
  struct MenuEntry ViewMenu[] =
    { { "Auswählen",    SelectMenu },
      { "Schließen",    HandleClose },
      { "Zurück",    0 } };

  HandleMenu( "Ansicht", ViewMenu, ELEMENTS( ViewMenu ) );
}

int main()
{
  struct MenuEntry MainMenu[] =
     { { "Datei",       DateiMenu },
       { "Bearbeiten",  BearbeitenMenu },
       { "Ansicht",     AnsichtMenu },
       { "Beenden",     0 } };

  HandleMenu( "Hauptmenue", MainMenu, ELEMENTS( MainMenu ) );
}