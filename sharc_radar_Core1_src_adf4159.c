/*
 * adf4159.c
 *
 *  Created on: 12.07.2017
 *      Author: roehr
 */

#include <builtins.h>
#include <sru.h>
#include <stdio.h>
#include <sys/platform.h>

#include "adf4159.h"
#include "init.h"
#include "ramp.h"
#include "rf_power.h"
#include "some_defines.h"
#include "spi.h"
#include "timers.h"

#define PLL_F_CLOCK				SYSTEM_CLOCK_ADF4159
#define R2_R_COUNTER_VALUE		1L
#define R2_REF_IN_DOUBLER_VALUE	0L
#define R2_REFERENCE_DIV2_VALUE	0L
#define PLL_REF_IN				SYSTEM_CLOCK_ADF4159
#define PLL_F_PFD				((double)PLL_REF_IN * (1+R2_REF_IN_DOUBLER_VALUE) / (R2_R_COUNTER_VALUE * (1 + R2_REFERENCE_DIV2_VALUE)))
#define CLK1_DIV_VALUE			2L
#define CLK2_DIV_VALUE			1L
#define PD_POLARITY				0

static uint32_t extRfPrescaler = 64;
static float64_t constCalcInt;
static float64_t constCalcFrac;
static bool adf4159Programmed  = false;


static void adf4159CalcIntFrac(float64_t frequency, struct adf4159State *pState);
static void adf4159CalcSweepParameters(const struct adf4159State *pState, struct ramp *ramp);
static void adf4159PrintSweepInformation(const struct adf4159State *pState);
static void adf4159Write(uint32_t adfAddress, uint32_t data);
static void adf4159ProgramRampSettings(const struct ramp *ramp, enum sweep sweep);


int32_t round32s(float64_t value)
{
	if (value > 0)
		return value + 0.5L;
	else
		return value - 0.5L;
}

uint32_t round32u(float64_t value)
{
	return value + 0.5L;
}

/**
 * \brief display sweep information
 *
 * This function calculates the sweep parameter (start frequency, stop frequency, duration)
 * from the PLL settings and prints the results. PLL settings are also shown.
 *
 **/
static void adf4159PrintSweepInformation(const struct adf4159State *pState)
{
	struct ramp ramp;
	adf4159CalcSweepParameters(pState, &ramp);	// get sweep parameters
	printf("    fStart = %13.9f GHz\r\n     fStop = %13.9f GHz\r\n     tRamp = %12.9f ms\r\n", (double)(ramp.fStart / 1.0e9L), (double)(ramp.fStop / 1.0e9L), (double)(ramp.tRamp * 1.0e3L));
	printf("   clk1Div = %lu\r\n      step = %lu\r\n       dev = %d (%ld)\r\n", (unsigned long)pState->clk1Div, (unsigned long)pState->stepWord, (int)pState->deviationWord, (long)pState->deviationWord);
	printf("       int = %lu\r\n   fracMSB = %lu\r\n   fracLSB = %lu\r\n", (unsigned long)pState->intValue, (unsigned long)pState->fracMsb, (unsigned long)pState->fracLsb);
}


/**
 * \brief intialize PLL state according to given sweep parameters
 *
 * This function calculates the PLL settings required to generate a sweep according to the
 * given start frequency, stop frequency and duration.
 *
 **/
void adf4159ConfigRamp(struct ramp_config *ramp_config, struct ramp *ramp)
{
	float64_t devWord		= 0.0L;
	float64_t stepWord		= 0.0L;
	float64_t correction	= 1.0L;
	unsigned int count 		= 0;

	adf4159CalcIntFrac(ramp_config->fStart, &ramp->adf4159State[UP]);

	ramp->adf4159State[UP].clk1Div = CLK1_DIV_VALUE;

	for (count = 0; count < 10 && (ABS(devWord) < 50 || stepWord < 50); count++) {
		// hack sr: check if/how this is stable ...; maybe us constant values for low bandwidth sweeps and only calc for max BW
		stepWord = ramp_config->tRamp * (float64_t)PLL_F_PFD / (float64_t)(ramp->adf4159State[UP].clk1Div * CLK2_DIV_VALUE);
		devWord = (ramp_config->fStop - ramp_config->fStart) / ((float64_t)PLL_F_PFD / (1L<<25) * extRfPrescaler) / stepWord;
		if (ABS(devWord) < 50) {
			correction = 50.0L / ABS(devWord);
			ramp->adf4159State[UP].clk1Div *= correction;
		}
	}

	stepWord = ramp_config->tRamp * (float64_t)PLL_F_PFD / (float64_t)(ramp->adf4159State[UP].clk1Div * CLK2_DIV_VALUE);
	devWord = (ramp_config->fStop - ramp_config->fStart) / ((float64_t)PLL_F_PFD / (1L<<25) * extRfPrescaler) / stepWord;

	ramp->adf4159State[UP].stepWord = round32u(stepWord);
	ramp->adf4159State[UP].deviationWord = round32s(devWord);
	ramp->adf4159State[UP].stepWord &= ((1L<<20)-1);		// limit stepWord to 20 bit
	ramp->adf4159State[UP].deviationWord &= ((1L<<16)-1);	// limit deviationWord to 16 bit
	ramp->adf4159State[UP].clk1Div &= ((1L<<12)-1);

	adf4159CalcSweepParameters(&ramp->adf4159State[UP], ramp);
	rampDeriveValues(ramp);

	adf4159CalcIntFrac(ramp->fStop, &ramp->adf4159State[DOWN]);					// set start frequency of reversed sweep using calculated end frequency
	ramp->adf4159State[DOWN].stepWord = ramp->adf4159State[UP].stepWord;						// use same number of steps
	ramp->adf4159State[DOWN].clk1Div = ramp->adf4159State[UP].clk1Div;							// use same clock divider
	ramp->adf4159State[DOWN].deviationWord = -(int16_t)ramp->adf4159State[UP].deviationWord;	// invert sweep direction
	ramp->adf4159State[DOWN].deviationWord &= ((1L<<16)-1);							// limit deviationWord to 16 bit

	if (true) {
		printf("\r\nInitialized Sweep:\r\n");
		adf4159PrintSweepInformation(&ramp->adf4159State[UP]);
		printf("\r\nReversed Sweep:\r\n");
		adf4159PrintSweepInformation(&ramp->adf4159State[DOWN]);
	}

	if (!adf4159Programmed) {
		adf4159ProgramRampSettings(ramp, UP);
	}
}

void adf4159PrepareResponseRamp(struct ramp *ramp_in, struct ramp *ramp_out, float64_t offsets[2])
{
	adf4159CalcIntFrac(ramp_in->fStart + offsets[UP], &ramp_out->adf4159State[UP]);

	ramp_out->adf4159State[UP].clk1Div = ramp_in->adf4159State[UP].clk1Div;
	ramp_out->adf4159State[UP].stepWord = ramp_in->adf4159State[UP].stepWord;
	ramp_out->adf4159State[UP].deviationWord = ramp_in->adf4159State[UP].deviationWord;

	//adf4159CalcSweepParameters(&ramp->adf4159State[UP], ramp);

	adf4159CalcIntFrac(ramp_in->fStop + offsets[DOWN], &ramp_out->adf4159State[DOWN]);					// set start frequency of reversed sweep using calculated end frequency
	ramp_out->adf4159State[DOWN].clk1Div = ramp_in->adf4159State[DOWN].clk1Div;
	ramp_out->adf4159State[DOWN].stepWord = ramp_in->adf4159State[DOWN].stepWord;
	ramp_out->adf4159State[DOWN].deviationWord = ramp_in->adf4159State[DOWN].deviationWord;
}
/**
 * \brief calculate integer and fractional value of PLL from given frequency
 *
 * This function calculates the integer and fractional values which must be programmed
 * to the PLL to set the output frequency of the radar to the given value.
 *
 **/
static void adf4159CalcIntFrac(float64_t frequency, struct adf4159State *pState)
{
	uint32_t fracValue;
	pState->intValue = frequency * constCalcInt;
	fracValue = (int64_t)(frequency * constCalcFrac + 0.5) - ((int64_t)(pState->intValue)<<(PLL_FRAC_MSBS+PLL_FRAC_LSBS));
	pState->fracMsb = fracValue >> PLL_FRAC_LSBS;
	pState->fracLsb = fracValue & ((1L<<PLL_FRAC_LSBS)-1);
}

/**
 * \brief calculate sweep settings from PLL parameters
 *
 * This function calculates and returns the sweep parameters from the given PLL parameters
 *
 **/
static void adf4159CalcSweepParameters(const struct adf4159State *pState, struct ramp *ramp)
{
	ramp->fStart = (float64_t)( (pState->fracMsb << PLL_FRAC_LSBS) | pState->fracLsb );
	ramp->fStart /= (float64_t)(1L<<(PLL_FRAC_LSBS + PLL_FRAC_MSBS));
	ramp->fStart += pState->intValue;
	ramp->fStart *= (float64_t)PLL_F_PFD;
	ramp->fStart *= (float64_t)extRfPrescaler;

	ramp->fStop = (float64_t)(int16_t)pState->deviationWord * (float64_t)pState->stepWord;
	ramp->fStop /= (float64_t)(1L<<(PLL_FRAC_LSBS + PLL_FRAC_MSBS));
	ramp->fStop *= (float64_t)PLL_F_PFD;
	ramp->fStop *= (float64_t)extRfPrescaler;
	ramp->fStop += ramp->fStart;

	ramp->tRamp = (float64_t)pState->stepWord * (float64_t)(pState->clk1Div * CLK2_DIV_VALUE) / (float64_t)PLL_F_PFD;
}

/**
 * \brief write data to PLL
 *
 * This function transmits data to the PLL via the SPI interface.
 *
 **/
static void adf4159Write(uint32_t adfAddress, uint32_t data)
{
	spiSelect(ADFPLL);	//select ADFPLL during the writing process

	data = data & 0xFFFFFFF8;
	adfAddress = adfAddress & 0x00000007;

	spiTxUint32(data | adfAddress);

	spiSelect(NONE);	//deselect ADFPLL after the writing process; also waits for TX to finish
}

/**
 * \brief initialize PLL
 *
 * This function sets I/O pins and programs the PLL registers to the initial settings.
 *
 **/
void adf4159Init(void)
{
	struct ramp ramp;
	struct adf4159State *state;
	INIT_DEPENDENCIES();

	rfPowerInit();
	spiInit();
	timersInit();
	rampInit();

	INIT_SELF();
	//Set and initialize the GPIO Pins for the ADF 4159 with the TI DSP
	extRfPrescaler = 32;

	constCalcInt = 1.0L / (float64_t)extRfPrescaler / (float64_t)PLL_F_PFD;
	constCalcFrac = 1.0L / (float64_t)extRfPrescaler / (float64_t)PLL_F_PFD * (float64_t)(1L<<(PLL_FRAC_MSBS+PLL_FRAC_LSBS));

	// initializing PLL_CE
	SRU2(HIGH, DAI1_PBEN01_I);
	SRU2(HIGH, DAI1_PB01_I);

	// TR_EN is initialized in rf_power.c
	// PLL_LE is initialized in spi.c
	// PLL_TRG is initialized in timers.c

	state = &ramp.adf4159State[UP];
	state->stepWord = 1;
	state->deviationWord = 0;
	state->clk1Div = 1;
	// TODO: choose better frequency value
	state->fracLsb = 3823;
	state->fracMsb = 1911;
	state->intValue = 25;
	adf4159ProgramRampSettings(&ramp, UP);

	INIT_DONE();
}

#define R7_SHIFT_TX_DATA_TRIGGER		20
#define R6_SHIFT_STEP_WORD				3
#define R6_SHIFT_STEP_SEL				23
#define R5_SHIFT_DEVIATION_WORD			3
#define R5_SHIFT_DEV_SEL				23
#define R4_SHIFT_LE_SEL					31
#define R4_SHIFT_CLK_DIV_MODE			19
#define R4_SHIFT_CLK2_DIV_VALUE			7
#define R4_SHIFT_CLK_DIV_SEL			6
#define R3_SHIFT_NEG_BLEED_CURRENT		22
#define R3_SHIFT_RESERVED_1				17
#define R3_SHIFT_LOL					16
#define R3_SHIFT_N_SEL					15
#define R3_SHIFT_SIGMA_DELTA_RESET		14
#define R3_SHIFT_RAMP_MODE				10
#define R3_SHIFT_LDP					7
#define R3_SHIFT_PD_POL					6
#define R2_SHIFT_CP_CURRENT_SETTING		24
#define R2_SHIFT_RDIV2					21
#define R2_SHIFT_REF_DOUBLER			20
#define R2_SHIFT_R_COUNTER				15
#define R2_SHIFT_CLK1_DIV_VALUE			3
#define R1_SHIFT_LSB_FRACTIONAL_VALUE	15
#define R0_SHIFT_RAMP_ON				31
#define R0_SHIFT_INTEGER_VALUE			15
#define R0_SHIFT_MSB_FRACTIONAL_VALUE	3

/*
static void blip(void)
{
	int i;
	timersTimer2Signal(TIMER_SIGNAL_HIGH);
	for (i = 0; i < 10000; i++)
		NOP();
	timersTimer2Signal(TIMER_SIGNAL_LOW);
}

static void sleep(void)
{
// sleep: 100000 == 2.5ms
	int i;
	for (i = 0; i < 100000; i++)
		NOP();
}
*/


/**
 * \brief update ramp settings (fast mode)
 *
 * This function rewrites only the PLL registers which are required to change the
 * sweep rate of the radar signals. This is faster then a complete reinitialization.
 *
 **/
void adf4159UpdateRampSettingsFast(const struct ramp *ramp, enum sweep sweep)
{
	const struct adf4159State *pState = &ramp->adf4159State[sweep];

	// stop ramping and switch to cw
	adf4159Write(PLL_REG_R5_DEVIATION, 0L << R5_SHIFT_DEVIATION_WORD);
	adf4159Write(PLL_REG_R1_LSB_FRAC, (pState->fracLsb << R1_SHIFT_LSB_FRACTIONAL_VALUE));
	adf4159Write(PLL_REG_R0_FRAC_INT, (pState->intValue << R0_SHIFT_INTEGER_VALUE)
									| (pState->fracMsb << R0_SHIFT_MSB_FRACTIONAL_VALUE));

	// trigger once to avoid automatic ramp start after programming registers
	timersTimer2Signal(TIMER_SIGNAL_HIGH);
	timersTimer2Signal(TIMER_SIGNAL_LOW);
	timersTimer2Signal(TIMER_SIGNAL_PERIPHERAL);

	adf4159Write(PLL_REG_R6_STEP, pState->stepWord << R6_SHIFT_STEP_WORD);
	adf4159Write(PLL_REG_R5_DEVIATION, pState->deviationWord << R5_SHIFT_DEVIATION_WORD);
	adf4159Write(PLL_REG_R2_R_DIVIDER, (7L << R2_SHIFT_CP_CURRENT_SETTING)
									| (R2_REFERENCE_DIV2_VALUE << R2_SHIFT_RDIV2)
									| (R2_REF_IN_DOUBLER_VALUE << R2_SHIFT_REF_DOUBLER)
									| (R2_R_COUNTER_VALUE << R2_SHIFT_R_COUNTER)
									| (pState->clk1Div << R2_SHIFT_CLK1_DIV_VALUE));
	adf4159Write(PLL_REG_R0_FRAC_INT, (1UL << R0_SHIFT_RAMP_ON)
									| (pState->intValue << R0_SHIFT_INTEGER_VALUE)
									| (pState->fracMsb << R0_SHIFT_MSB_FRACTIONAL_VALUE));
}

/**
 * \brief program sweep settings to PLL
 *
 * This function programs all registers of the PLL according to the given sweep/PLL parameters
 *
 **/
void adf4159ProgramRampSettings(const struct ramp *ramp, enum sweep sweep)
{
	const struct adf4159State *pState = &ramp->adf4159State[sweep];

	timersTimer2Signal(TIMER_SIGNAL_LOW);

	// stop ramping and switch to cw
	adf4159Write(PLL_REG_R5_DEVIATION, 0L << R5_SHIFT_DEVIATION_WORD);
	adf4159Write(PLL_REG_R1_LSB_FRAC, (pState->fracLsb << R1_SHIFT_LSB_FRACTIONAL_VALUE));
	adf4159Write(PLL_REG_R0_FRAC_INT, (pState->intValue << R0_SHIFT_INTEGER_VALUE)
									| (pState->fracMsb << R0_SHIFT_MSB_FRACTIONAL_VALUE));

	// trigger once to avoid automatic ramp start after programming registers
	timersTimer2Signal(TIMER_SIGNAL_HIGH);
	timersTimer2Signal(TIMER_SIGNAL_LOW);
	timersTimer2Signal(TIMER_SIGNAL_PERIPHERAL);

	adf4159Write(PLL_REG_R7_DELAY, 1L << R7_SHIFT_TX_DATA_TRIGGER);
	adf4159Write(PLL_REG_R6_STEP, pState->stepWord << R6_SHIFT_STEP_WORD);
	adf4159Write(PLL_REG_R6_STEP, 1L << R6_SHIFT_STEP_SEL); // second step register is not used
	adf4159Write(PLL_REG_R5_DEVIATION, pState->deviationWord << R5_SHIFT_DEVIATION_WORD);
	adf4159Write(PLL_REG_R5_DEVIATION, 1L << R5_SHIFT_DEV_SEL);				// second deviation register is not used
	adf4159Write(PLL_REG_R4_CLOCK, (1L << R4_SHIFT_LE_SEL)					// enable synchronization of LE pin; avoids timing problems in chip
								| (3L << R4_SHIFT_CLK_DIV_MODE)				// Clock is used for ramps
								| (CLK2_DIV_VALUE << R4_SHIFT_CLK2_DIV_VALUE));
	adf4159Write(PLL_REG_R4_CLOCK, (1L << R4_SHIFT_LE_SEL)					// enable synchronization of LE pin; avoids timing problems in chip
								| (3L << R4_SHIFT_CLK_DIV_MODE)				// Clock is used for ramps
								| (CLK2_DIV_VALUE << R4_SHIFT_CLK2_DIV_VALUE)
								| (1L << R4_SHIFT_CLK_DIV_SEL));			// written to second clock register
	adf4159Write(PLL_REG_R3_FUNCTION, (6L << R3_SHIFT_NEG_BLEED_CURRENT)	// negative bleed current 454.7 uA; but not actually enabled
									| (1L << R3_SHIFT_RESERVED_1)			// reserved; must be 1
									| (1L << R3_SHIFT_LOL)					// disable "loss of lock" feature; not using it makes things supposedly more robust
									| (1L << R3_SHIFT_N_SEL)				// 1 = delay N word load by 4 cycles; makes transition between sweeps smoother
									| (3L << R3_SHIFT_RAMP_MODE)			// ramp mode 3: single ramp burst
									| (1L << R3_SHIFT_LDP)					// "lock detect precision"; writing 1makes things supposedly more robust
									| (1L << R3_SHIFT_SIGMA_DELTA_RESET)	// 1 = disable sigma-delta reset on every write
									| (PD_POLARITY << R3_SHIFT_PD_POL));	// phase detector polarity; depends on VCO polarity
	adf4159Write(PLL_REG_R2_R_DIVIDER, (7L << R2_SHIFT_CP_CURRENT_SETTING)	// CP current 2.5 mA
									| (R2_REFERENCE_DIV2_VALUE << R2_SHIFT_RDIV2)
									| (R2_REF_IN_DOUBLER_VALUE << R2_SHIFT_REF_DOUBLER)
									| (R2_R_COUNTER_VALUE << R2_SHIFT_R_COUNTER)
									| (pState->clk1Div << R2_SHIFT_CLK1_DIV_VALUE));
	adf4159Write(PLL_REG_R1_LSB_FRAC, (pState->fracLsb << R1_SHIFT_LSB_FRACTIONAL_VALUE));
	adf4159Write(PLL_REG_R0_FRAC_INT, (1UL << R0_SHIFT_RAMP_ON)				// enable ramps
									| (pState->intValue << R0_SHIFT_INTEGER_VALUE)
									| (pState->fracMsb << R0_SHIFT_MSB_FRACTIONAL_VALUE));

//	ioPrintf(" programmed: INT: %lu, MSB: %lu, LSB: %lu, step: %lu, dev: %ld\r\n", pState->intValue, pState->fracMsb, pState->fracLsb, pState->stepWord, pState->deviationWord);
}
