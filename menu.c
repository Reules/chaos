#include <stdio.h>

#define DAC_START 		8191
#define DAC_MAX 		16383
#define DAC_MIN 		0
#define BGT_CONF_SPI	6125

extern UART_HandleTypeDef huart1;
extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi3;

char uart1DataRx;
static char uart1BufferTx[100];
static uint8_t uart1TxCplt;
static uint8_t bufferLen;

int optionSelect;
uint16_t spi3DataTx = DAC_START; 
uint16_t spi1DataTxBgt = BGT_CONF_SPI;

struct rs232_menu{
	char menuName[50];
	int menuLength;
	char *menuOption[10];
};

struct rs232_menu mainMenu = {"Main Menu",
	3,
	{"DAC value setting",
	"ADF4159 configuration",
	"BGT24MTR11 configuration"}
	};
	
struct rs232_menu dacMenu = {"DAC value setting",
	5,
	{"+1",
	 "-2",
	 "x2",
	 "/2",
	 "back"
	}
};

struct rs232_menu adf4159Menu = {"ADF4159 configuration",
	2,
	{"PLL configuration",
	 "back"
	}
};

struct rs232_menu bgt24Menu = {"BGT24MTR11 configuration",
	2,
	{"BGT24 SPI configuration",
	 "back"
	}
};
	
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	if (huart->Instance == USART1)  //current UART
	{
		uart1TxCplt = 1;          //transfer complete, data is ready to read
//		HAL_UART_Receive_IT(&huart1, (uint8_t *) &uart1DataRx, 1); //activate UART receive interrupt every time
	}
}	
	
/*Submenu*/
static dacMenuExcution(const struct rs232_menu *menu);
static adf4159MenuExcution(const struct rs232_menu *menu);
static bgt24MenuExcution(const struct rs232_menu *menu);

/*the functions of submenu*/
static dacAddtion(void);
static dacSubtraction(void);
static dacMultiplication(void);
static dacDivision(void);

static adf4159Spi1PllConfig(void);

static bgt24Spi1Config(void);

/*the tranmission functions*/
static Spi3DacTran(void);
static Spi1Adf4159Tran(const uint16_t *data_msb,const uint16_t *data_lsb);
static Spi1Bgt24Tran(void);

static adf4159Spi1Tx(uint32_t adfAddress, uint32_t data);

	
void mainMenuExcution(const struct rs232_menu *menu)
{
	//print function through uart1
	sprintf(uart1BufferTx, "%s\n", menu->menuName);
	HAL_UART_Transmit(&huart1, (uint8_t *)uart1BufferTx, bufferLen, 1000);
	
	for(int i=0; i< menu->menuLength; i++){
		bufferLen = sprintf(uart1BufferTx, "%d: %s\n", i+1,menu->menuOption[i]);
		HAL_UART_Transmit(&huart1, (uint8_t *)uart1BufferTx, bufferLen, 1000);		
	};
	
	HAL_UART_Receive_IT(&huart1, (uint8_t *) &uart1DataRx, 1); 	//active the uart1 to accept interrupt
	while (!uart1TxCplt) {};									//stop the program until the interrupt is accepted
	
	optionSelect = uart1DataRx - "0";							//convert the char to int
	uart1TxCplt = 0;											//reset transfer complete variable	
	
	switch(optionSelect){
		case 1:
			dacMenuExcution(&dacMenu);
			break;
		case 2:
			adf4159MenuExcution(&adf4159Menu);
			break;
		case 3:
			bgt24MenuExcution(&bgt24Menu);
	}
}

static void dacMenuExcution(const struct rs232_menu *menu)
{
	//print function through uart1
	sprintf(uart1BufferTx, "%s\n", menu->menuName);
	HAL_UART_Transmit(&huart1, (uint8_t *)uart1BufferTx, bufferLen, 1000);
	
	for(int i=0; i< menu->menuLength; i++){
		bufferLen = sprintf(uart1BufferTx, "%d: %s\n", i+1,menu->menuOption[i]);
		HAL_UART_Transmit(&huart1, (uint8_t *)uart1BufferTx, bufferLen, 1000);		
	};
	
	HAL_UART_Receive_IT(&huart1, (uint8_t *) &uart1DataRx, 1); 	//active the uart1 to accept interrupt
	while (!uart1TxCplt) {};									//stop the program until the interrupt is accepted
	
	optionSelect = uart1DataRx - "0";							//convert the char to int
	uart1TxCplt = 0;											//reset transfer complete variable	
	
	switch(optionSelect){
		case 1:
			dacAddtion();
			break;
		case 2:
			dacSubtraction();
			break;
		case 3:
			dacMultiplication();
			break;
		case 4:
			dacDivision();
			break;
		case 5:
			mainMenuExcution(&mainMenu);
	}	
}

static void adf4159MenuExcution(const struct rs232_menu *menu){
	//print function through uart1
	sprintf(uart1BufferTx, "%s\n", menu->menuName);
	HAL_UART_Transmit(&huart1, (uint8_t *)uart1BufferTx, bufferLen, 1000);
	
	for(int i=0; i< menu->menuLength; i++){
		bufferLen = sprintf(uart1BufferTx, "%d: %s\n", i+1,menu->menuOption[i]);
		HAL_UART_Transmit(&huart1, (uint8_t *)uart1BufferTx, bufferLen, 1000);		
	};
	
	HAL_UART_Receive_IT(&huart1, (uint8_t *) &uart1DataRx, 1); 	//active the uart1 to accept interrupt
	while (!uart1TxCplt) {};									//stop the program until the interrupt is accepted
	
	optionSelect = uart1DataRx - "0";							//convert the char to int
	uart1TxCplt = 0;											//reset transfer complete variable	
	
	switch(optionSelect){
		case 1:
			adf4159Spi1PllConfig();
			break;
		case 2:
			mainMenuExcution(&mainMenu);
	}
}

static void bgt24MenuExcution(const struct rs232_menu *menu){
	//print function through uart1
	sprintf(uart1BufferTx, "%s\n", menu->menuName);
	HAL_UART_Transmit(&huart1, (uint8_t *)uart1BufferTx, bufferLen, 1000);
	
	for(int i=0; i< menu->menuLength; i++){
		bufferLen = sprintf(uart1BufferTx, "%d: %s\n", i+1,menu->menuOption[i]);
		HAL_UART_Transmit(&huart1, (uint8_t *)uart1BufferTx, bufferLen, 1000);		
	};
	
	HAL_UART_Receive_IT(&huart1, (uint8_t *) &uart1DataRx, 1); 	//active the uart1 to accept interrupt
	while (!uart1TxCplt) {};									//stop the program until the interrupt is accepted
	
	optionSelect = uart1DataRx - "0";							//convert the char to int
	uart1TxCplt = 0;											//reset transfer complete variable	
	
	switch(optionSelect){
		case 1:
			bgt24Spi1Config();
		case 2:
			mainMenuExcution(&mainMenu);
	}
}


static void dacAddtion(void){
	spi3DataTx = spi3DataTx + 1;
	if(spi3DataTx > DAC_MAX){
		bufferLen = sprintf(uart1BufferTx, "\out of range!");
		HAL_UART_Transmit(&huart1, (uint8_t *)uart1BufferTx, bufferLen, 1000);	
		spi3DataTx = spi3DataTx - 1;
	}
	Spi3DacTran();
	bufferLen = sprintf(uart1BufferTx,"\rcurrent DAC value is: %d", spi3DataTx);
	HAL_UART_Transmit(&huart1, (uint8_t *)uart1BufferTx, bufferLen, 1000);	
}

static void dacSubtraction(void){
	spi3DataTx = spi3DataTx - 1;
	if (spi3DataTx < DAC_MIN || spi3DataTx > DAC_MAX) {
		bufferLen = sprintf(uart1BufferTx, "\out of range!");
		HAL_UART_Transmit(&huart1, (uint8_t *)uart1BufferTx, bufferLen, 1000);	
		spi3DataTx = spi3DataTx + 1;
	}
	Spi3DacTran();
	bufferLen = sprintf(uart1BufferTx,"\rcurrent DAC value is: %d", spi3DataTx);
	HAL_UART_Transmit(&huart1, (uint8_t *)uart1BufferTx, bufferLen, 1000);
}

static void dacMultiplication(void){
	spi3DataTx = spi3DataTx * 2;
	if (spi3DataTx > DAC_MAX) {
	bufferLen = sprintf(uart1BufferTx, "\out of range!");
		HAL_UART_Transmit(&huart1, (uint8_t *)uart1BufferTx, bufferLen, 1000);	
		spi3DataTx = spi3DataTx / 2;
	}
	Spi3DacTran();
	bufferLen = sprintf(uart1BufferTx,"\rcurrent DAC value is: %d", spi3DataTx);
	HAL_UART_Transmit(&huart1, (uint8_t *)uart1BufferTx, bufferLen, 1000);
}

static void dacDivision(void){
	spi3DataTx = spi3DataTx / 2;
	Spi3DacTran();
	printf("\rcurrent DAC value is: %d", spi3DataTx);
}

static void adf4159Spi1PllConfig(void){
	adf4159Spi1Tx(PLL_REG_R7_DELAY, 1L << R7_SHIFT_TX_DATA_TRIGGER); 					//TX_DATA_TRIGGER enabled
	adf4159Spi1Tx(PLL_REG_R6_STEP, 0L << R6_SHIFT_STEP_SEL);
	adf4159Spi1Tx(PLL_REG_R6_STEP, 1L << R6_SHIFT_STEP_SEL); 							// second step register is not used
	adf4159Spi1Tx(PLL_REG_R5_DEVIATION, 0L << R5_SHIFT_DEV_SEL);
	adf4159Spi1Tx(PLL_REG_R5_DEVIATION, 1L << R5_SHIFT_DEV_SEL);							// second deviation register is not used
	adf4159Spi1Tx(PLL_REG_R4_CLOCK, 0L << R4_SHIFT_CLK_DIV_SEL);
	adf4159Spi1Tx(PLL_REG_R4_CLOCK, 1L << R4_SHIFT_CLK_DIV_SEL);
	adf4159Spi1Tx(PLL_REG_R3_FUNCTION, (6L << R3_SHIFT_NEG_BLEED_CURRENT)				// negative bleed current 454.7 uA; but not actually enabled
									| (1L << R3_SHIFT_RESERVED_1)						// reserved; must be 1
									| (1L << R3_SHIFT_LOL)								// disable "loss of lock" feature; not using it makes things supposedly more robust
									| (1L << R3_SHIFT_N_SEL)							// 1 = delay N word load by 4 cycles; makes transition between sweeps smoother
									| (1L << R3_SHIFT_LDP)								// "lock detect precision"; writing 1makes things supposedly more robust
									| (1L << R3_SHIFT_SIGMA_DELTA_RESET)				// 1 = disable sigma-delta reset on every write
									| (0L << R3_SHIFT_PD_POL));							// phase detector polarity is negativ
	adf4159Spi1Tx(PLL_REG_R2_R_DIVIDER, (7L << R2_SHIFT_CP_CURRENT_SETTING)				// CP current 2.5 mA
									| (R2_REFERENCE_DIV2_VALUE << R2_SHIFT_RDIV2)
									| (R2_REF_IN_DOUBLER_VALUE << R2_SHIFT_REF_DOUBLER)
									| (R2_R_COUNTER_VALUE << R2_SHIFT_R_COUNTER)
									| (0L << R2_SHIFT_CLK1_DIV_VALUE));
	adf4159Spi1Tx(PLL_REG_R1_LSB_FRAC, (0L << R1_SHIFT_LSB_FRACTIONAL_VALUE));			//Fraction LSB is 0
	adf4159Spi1Tx(PLL_REG_R0_FRAC_INT, (23L << R0_SHIFT_INTEGER_VALUE)					//Int value is 23
									| (1792L << R0_SHIFT_MSB_FRACTIONAL_VALUE));		//Fraction MSB is 1792
}

static void bgt24Spi1Config(void){
	Spi1Bgt24Tran();
}

static void adf4159Spi1Tx(uint32_t adfAddress, uint32_t data){
	uint32_t data_address;
	data = data & 0xFFFFFFF8;
	adfAddress = adfAddress & 0x00000007;
	data_address = data | adfAddress;
	uint16_t data_msb = data_address >> 16;
	uint16_t data_lsb = data_address;
	Spi1Adf4159Tran(&data_msb, &data_lsb);
}

static void Spi3DacTran(void){
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi3, (uint8_t *)&spi3DataTx, 1, 5000);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET)
}

static void Spi1Adf4159Tran(const uint16_t *data_msb,const uint16_t *data_lsb){
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, (uint8_t*) data_msb, 1, 5000); 
	HAL_SPI_Transmit(&hspi1, (uint8_t*) data_lsb, 1, 5000);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
}

static void Spi1Bgt24Tran(void){
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, (uint8_t*) &spi1DataTxBgt, 1, 5000);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
}

int main(){
	menuExcution(&mainMenu);
	return 0;
}